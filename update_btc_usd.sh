#!/bin/bash
fdata_btc=./btc.csv
fdata_eth=./eth.csv
fig_btc=./btc.png
fig_eth=./eth.png

url_btc="http://data.bitcoinity.org/export_data.csv?c=e&currenc=USD&data_type=price&r=day&t=l&timespan=all"
url_eth="https://etherscan.io/chart/etherprice?output=csv"

#--- ETH
# download data
wget ${url_btc} -O ${fdata_btc} 
# BTC: make figure 
./tt.py -- btc -fi ${fdata_btc} -ff ${fig_btc} \
    && echo -e "\n [+] generated figure: ${fig_btc}\n" \
    || echo -e "\n [-] sthin'g went wrong w/ BTC!\n"

#--- BTC
# download data
wget ${url_eth} -O ${fdata_eth}
# ETH: make figure 
./tt.py -- eth -fi ${fdata_eth} -ff ${fig_eth} \
    && echo -e "\n [+] generated figure: ${fig_eth}\n" \
    || echo -e "\n [-] sthing wen't wrong w/ ETH!\n"

#EOF
