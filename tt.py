#!/usr/bin/env ipython
# -*- coding: utf-8 -*-
import numpy as np
from pylab import figure, close, show
from datetime import datetime, timedelta
import argparse, sys, os
import funcs as ff


#--- retrieve args
parser = argparse.ArgumentParser(
formatter_class=argparse.ArgumentDefaultsHelpFormatter,
description="""
Download && plot crypto-coin historical data.
""",
)
# subparsers
subparsers = parser.add_subparsers(description=
    """
    Use one of the submodules below.
    """,
    )
# config the subparsers
for mod_name in ['btc', 'eth']:
    # grab the class
    mod = getattr(ff, 'fparser__'+mod_name)()
    # grab the parser of that class
    subparser_ = subparsers.add_parser(
    mod_name, 
    help=mod.help,
    parents=[mod.parser], 
    formatter_class=argparse.ArgumentDefaultsHelpFormatter
    )
    # all subparser will have this options in common:
    subparser_.add_argument(
    '-fi', '--fname_inp',
    type=str,
    default="./coin.dat",
    help='historical value of BTC in USD',
    )
    subparser_.add_argument(
    '-ff', '--fname_fig',
    type=str,
    default="./coin_in_usd.png",
    help='output figure',
    )
    subparser_.add_argument(
    '-xlim', '--xlim',
    type=float,
    nargs=2,
    default=[2,None],
    metavar=('XMIN', 'XMAX'),
    help='x-axis limits. If you use this w/o using the --ylim option,  it automatically finds the y-axis limits for the specified window of --xlim.',
    )
    subparser_.add_argument(
    '-ylim', '--ylim',
    type=float,
    nargs=2,
    default=[None,None], # no limits
    metavar=('YMIN', 'YMAX'),
    help='y-axis limits.',
    )
    subparser_.add_argument(
    '-scl', '--scales',
    type=str,
    nargs=2,
    default=['log','log'],
    metavar=('XSCALE','YSCALE'),
    help="""
    scales for X and Y axis, respectively. Can be
    'linear' or 'log'.
    """,
    )
    subparser_.add_argument(
    '-ini', '--ini_date', 
    type=str, 
    action=ff.arg_to_datetime,
    default=None, #'01/01/2006',
    metavar=('IniDate'),
    help='initial date (in format DD/MM/YYYY) from which the relative time (x axis) will be built from; i.e. the zero value in x-axis is this reference date/time.',
    )
pa = parser.parse_args()


#--- build a time series from input files
# grab the name of the module selected in the CLI
mod_selected = sys.argv[1]
print("\n ---> Using %s\n" % mod_selected)
mod = getattr(ff,'fparser__'+mod_selected)()
# build the times series
# utc_o : utc-sec reference for 'time'
utc_o, tutc, time, price = getattr(mod, 'get_ts')(pa)


#--- plot
fig = figure(1, figsize=(6,4))
ax  = fig.add_subplot(111)

#ax.plot((t-t[1])/(86400.*30.), p, label='btc')
ax.plot(time, price, label='btc')
ax.grid(True, ls='--', lw=0.5, c='black')
ax.set_yscale(pa.scales[1])
# if we didn't specify the ylim, then deduce it only
# if we specified xlim:
if (pa.ylim == [None,None]) and (pa.xlim != [None,None]):
    cx     = time>=pa.xlim[0] if pa.xlim[0] is not None else True
    cx    &= time<=pa.xlim[1] if pa.xlim[1] is not None else True
    cx    &= np.ones(price.size, dtype=np.bool)
    ylim_min = np.nanmin(price[cx])
    ylim_max = np.nanmax(price[cx])

ax.set_ylim(pa.ylim if pa.ylim != [None,None] else \
    [ylim_min, ylim_max])


# si tmb quieres "x" en log
ax.set_xscale(pa.scales[0])
ax.set_xlim(pa.xlim)

ax.set_xlabel('months since '+ff.utc2date(utc_o).strftime('%d/%b/%Y'))
ax.set_ylabel('USD per %s' % mod_selected.upper())

ax.legend(loc='best')
#fig.show()
#show(fig)
fig.savefig(pa.fname_fig, dpi=200, bbox_inches='tight')
close(fig)

#EOF
