import numpy as np
from pylab import figure, close, show
from datetime import datetime, timedelta
import argparse
import pandas as pd

class fparser__btc(object):
    def __init__(self,):
        """
        First thing to do is build the parser
        """
        self.help = """
        Module to read/process CSV for BTC.
        """
        self.parser = parser = argparse.ArgumentParser(
        description="""this gral description...""", 
        add_help=False
        )

    def get_ts(self, pa):
        """
        Build && return time series
        Set to read this source:
        "http://data.bitcoinity.org/export_data.csv?c=e&currenc=USD&data_type=price&r=day&t=l&timespan=all"
        """
        dd = np.genfromtxt(pa.fname_inp, delimiter=',', converters={0: lambda s: str2utc(s)}, 
            encoding='utf-8', skip_header=1)

        t  = np.array([ _d[0] for _d in dd ])  # utc sec
        p  = np.array([ _d[-1] for _d in dd ]) # price
        utc_o = date2utc(pa.ini_date) if pa.ini_date is not None \
            else t[1]
        dt = (t-utc_o)/(86400.*30.)
        cc = dt>0.0

        return utc_o, t[cc], dt[cc], p[cc]

class fparser__eth(object):
    def __init__(self,):
        """
        First thing to do is build the parser
        """
        self.help = """
        Module to read/process CSV for ETH.
        """
        self.parser = parser = argparse.ArgumentParser(
        description="""this gral description...""", 
        add_help=False
        )

    def get_ts(self, pa):
        """
        Build  && return time series
        Set to read this source:
        "https://etherscan.io/chart/etherprice?output=csv"
        """
        #dd = np.genfromtxt(pa.fname_inp, delimiter=',', converters)
        # convert strings to the respective data-types
        convs = {
        0: lambda s: str2utc(s, "%m/%d/%Y"),
        1: lambda s: int(s),
        2: lambda s: float(s),
        }
        df = pd.read_csv(pa.fname_inp, sep=',', header=0, converters=convs)
        tutc  = df['UnixTimeStamp'].values
        price = df['Value'].values
        utc_o = date2utc(pa.ini_date) if pa.ini_date is not None \
            else tutc[0]
        dt = (tutc - utc_o)/(86400.*30.)
        cc = dt>0.0

        return utc_o, tutc[cc], dt[cc], price[cc]



def utc2date(t):
    date_utc = datetime(1970, 1, 1, 0, 0, 0, 0)
    date = date_utc + timedelta(days=(t/86400.))
    return date


def date2utc(date):
    date_utc = datetime(1970, 1, 1, 0, 0, 0, 0)
    utcsec = (date - date_utc).total_seconds() # [utc sec]
    return utcsec


def str2utc(_s, fmt="%Y-%m-%d %H:%M:%S UTC"):
    d = datetime.strptime(_s, fmt)
    return date2utc(d)


class arg_to_datetime(argparse.Action):
    """
    argparse-action to handle command-line arguments of 
    the form "dd/mm/yyyy" (string type), and converts
    it to datetime object.
    NOTE: can be used even when `nargs`>1.
    """
    def __init__(self, option_strings, dest, **kwargs):
        #if nargs is not None:
        #    raise ValueError("nargs not allowed")
        super(arg_to_datetime, self).__init__(option_strings, dest, **kwargs)
    def __call__(self, parser, namespace, values, option_string=None):
        #print '%r %r %r' % (namespace, values, option_string)
        #if len(values)==1:
        #elif len(values)>1:
        if type(values) is str:
            dd,mm,yyyy = map(int, values.split('/'))
            value = datetime(yyyy,mm,dd)
        elif type(values) is list:
            value = []
            for val in values:
                dd,mm,yyyy = map(int, val.split('/'))
                value += [ datetime(yyyy,mm,dd) ]
        setattr(namespace, self.dest, value)

#EOF
