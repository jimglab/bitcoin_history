## Historical value of BTC in USD

Run (it downloads a `.csv` and generates a figure):

    ./update_btc_usd.sh

---
Data from:

    http://data.bitcoinity.org/markets/price/5y/USD?c=e&r=day&t=l

Just:

    wget http://data.bitcoinity.org/export_data.csv\?c\=e\&currency\=USD\&data_type\=price\&r\=day\&t\=l\&timespan\=all btc.dat

<!--- EOF -->
