#!/usr/bin/env python
# -*- coding: utf-8 -*-
import numpy as np
from pylab import figure, close

# utc : utc time
# btc : average amount of BTC transacted in that day
utc, btc = np.loadtxt('./btc_trans.txt', unpack=1)
utc_ini = utc[0]

fig = figure(1, figsize=(9,4))
ax  = fig.add_subplot(111)

ax.plot((utc-utc_ini)/86400., btc, '-', ms=3)
ax.grid(1)
ax.set_yscale('log')

fig.savefig('./trans.png')
close(fig)


#EOF
